import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

public class Main {
    public static Scanner scanner = new Scanner(System.in);
    public static int totalBill = 0;
    public static ArrayList<OrderItem> orderedItems = new ArrayList<>();

    public static void main(String[] args) {
        mainMenu(totalBill, orderedItems);
    }

    private static void mainMenu(int totalBill, ArrayList<OrderItem> orderedItems) {
        ArrayList<MenuItem> menuItems = new ArrayList<>();
        initializeMenu(menuItems);

        System.out.println("===============================");
        System.out.println("Selamat Datang di Bundo Kanduang");
        System.out.println("===============================");
        boolean isOrdering = true;

        while (isOrdering) {
            printMenu(menuItems);
            int choice = 0;
            try {
                System.out.print("Masukkan pilihan Anda: ");
                choice = scanner.nextInt();
            } catch (InputMismatchException e) {
                System.out.println("Mohon maaf, input yang Anda masukkan tidak valid. Silakan masukkan angka.");
                scanner.next();
                continue;
            }

            switch (choice) {
                case 1:
                case 2:
                case 3:
                    int bill = orderItem(menuItems.get(choice - 1), scanner, orderedItems);
                    if (bill == 0) {
                        break;
                    }
                    totalBill += bill;
                    break;
                case 99:
                    confirmOrder(totalBill, orderedItems);
                    break;
                case 0:
                    isOrdering = false;
                    System.out.println("Terima kasih telah berkunjung");
                    System.exit(0);
                    break;
                default:
                    System.out.println("Pilihan tidak valid. Silakan pilih menu yang tersedia.");
                    break;
            }

            if (isOrdering) {
                System.out.print("Ingin memesan lagi? (Y/N): ");
                String continueOrder = scanner.next();
                if (!continueOrder.equalsIgnoreCase("Y")) {
                    isOrdering = false;
                }

            }
        }
        confirmOrder(totalBill, orderedItems);
    }

    private static void confirmOrder(int totalBill, ArrayList<OrderItem> orderedItems) {
        if (totalBill == 0 && orderedItems.isEmpty()) {
            System.out.println("Anda belum memesan apapun");
            mainMenu(totalBill, orderedItems);
            return;
        }

        System.out.println("===============================");
        System.out.println("Konfirmasi & Pembayaran");
        System.out.println("===============================");
        System.out.println(" ");
        for (OrderItem item : orderedItems) {
            System.out.println(item.getItemName() + "     " + item.getQuantity() + "     Rp" + item.getTotalPrice());
        }
        System.out.println("-------------------------------");
        System.out.println("Total Bill: Rp" + totalBill);
        System.out.println(" ");
        System.out.println("1. Konfirmasi dan Bayar");
        System.out.println("2. Kembali ke menu utama");
        System.out.println("3. Keluar aplikasi");
        int choice = 0;
        try {
            System.out.print("Pilih opsi: ");
            choice = scanner.nextInt();
        } catch (InputMismatchException e) {
            System.out.println("Mohon maaf, input yang Anda masukkan tidak valid. Silakan masukkan angka.");
            scanner.next();
            confirmOrder(totalBill, orderedItems);
            return;
        }

        switch (choice) {
            case 1:
                printBill(totalBill, orderedItems);
                break;
            case 2:
                mainMenu(totalBill, orderedItems);
                break;
            case 3:
                break;
            default:
                System.out.println("Pilihan tidak valid. Silakan pilih opsi yang tersedia.");
                confirmOrder(totalBill, orderedItems);
                break;
        }

        scanner.close();
    }

    private static void printBill(int totalBill, ArrayList<OrderItem> orderedItems) {
        System.out.println("===============================");
        System.out.println("Bundo Kanduang");
        System.out.println("===============================");
        System.out.println("Terima kasih sudah memesan di Bundo Kanduang");
        System.out.println(" ");
        System.out.println("Dibawah ini adalah pesanan anda");
        for (OrderItem item : orderedItems) {
            System.out.println(item.getItemName() + "     " + item.getQuantity() + "     Rp" + item.getTotalPrice());
        }
        System.out.println("-------------------------------");
        System.out.println("Total Bill: Rp" + totalBill);
        System.out.println(" ");
        System.out.println("===============================");
        System.out.println("Simpan struk ini sebagai Bukti pembayaran");
        System.out.println("===============================");
        System.out.println("Terima kasih telah datang!");

        saveReceipt(totalBill, orderedItems);

        scanner.close();
    }

    public static void initializeMenu(ArrayList<MenuItem> menuItems) {
        menuItems.add(new MenuItem("Nasi Padang", 20000));
        menuItems.add(new MenuItem("Nasi Rendang", 25000));
        menuItems.add(new MenuItem("Ayam Pop", 23000));
    }

    public static void printMenu(ArrayList<MenuItem> menuItems) {
        System.out.println("Menu:");
        for (int i = 0; i < menuItems.size(); i++) {
            System.out.println((i + 1) + ". " + menuItems.get(i).getName() + " - Rp" + menuItems.get(i).getPrice());
        }
        System.out.println("99. Pesan dan Bayar");
        System.out.println("0. Keluar Aplikasi");
        System.out.println("===============================");
    }

    public static int orderItem(MenuItem menuItem, Scanner scanner, List<OrderItem> orderedItems) {
        System.out.print("Berapa " + menuItem.getName() + " yang Anda pesan? ");
        System.out.println("(Input 0 untuk kembali)");
        int quantity = 0;
        try {
            System.out.print("Qty => ");
            quantity = scanner.nextInt();
        } catch (InputMismatchException e) {
            System.out.println("Mohon maaf, input yang Anda masukkan tidak valid. Silakan masukkan angka.");
            scanner.next();
            return 0;
        }

        for (OrderItem item : orderedItems) {
            if (item.getItemName().equalsIgnoreCase(menuItem.getName())) {
                item.setQuantity(item.getQuantity() + quantity);
                item.setTotalPrice(item.getTotalPrice() + (menuItem.getPrice() * quantity));
                return menuItem.getPrice() * quantity;
            }
        }

        orderedItems.add(new OrderItem(menuItem.getName(), menuItem.getPrice(), quantity));
        return menuItem.getPrice() * quantity;
    }

    static class MenuItem {
        private String name;
        private int price;

        public MenuItem(String name, int price) {
            this.name = name;
            this.price = price;
        }

        public String getName() {
            return name;
        }

        public int getPrice() {
            return price;
        }
    }

    static class OrderItem {
        private String itemName;

        private int quantity;
        private int totalPrice;

        public OrderItem(String itemName, int price, int quantity) {
            this.itemName = itemName;
            this.quantity = quantity;
            this.totalPrice = price * quantity;
        }

        public String getItemName() {
            return itemName;
        }

        public int getQuantity() {
            return quantity;
        }

        public int getTotalPrice() {
            return totalPrice;
        }

        public void setQuantity(int quantity) {
            this.quantity = quantity;
        }

        public void setTotalPrice(int totalPrice) {
            this.totalPrice = totalPrice;
        }
    }

    public static void saveReceipt(int totalBill, ArrayList<OrderItem> orderedItems) {
        try {
            FileWriter writer = new FileWriter("receipt.txt");
            writer.write("===============================\n");
            writer.write("Bundo Kanduang\n");
            writer.write("===============================\n");
            writer.write("Terima kasih sudah memesan di Bundo Kanduang\n\n");
            writer.write("Dibawah ini adalah pesanan anda:\n");
            for (OrderItem item : orderedItems) {
                writer.write(item.getItemName() + "\t" + item.getQuantity() + "\tRp" + item.getTotalPrice() + "\n");
            }
            writer.write("\n-------------------------------\n");
            writer.write("Total Bill: Rp" + totalBill + "\n\n");
            writer.write("===============================\n");
            writer.write("Simpan struk ini sebagai Bukti pembayaran\n");
            writer.write("===============================\n\n");
            writer.write("Terima kasih telah datang!\n");
            writer.close();
            System.out.println("Struk pembayaran telah disimpan sebagai 'receipt.txt'");
        } catch (IOException e) {
            System.out.println("Terjadi kesalahan dalam menyimpan struk pembayaran.");
            e.printStackTrace();
        }
    }
}
