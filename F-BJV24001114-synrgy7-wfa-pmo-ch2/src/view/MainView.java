package view;

import model.MenuItem;
import model.OrderItem;

import java.io.FileWriter;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

import static utils.StringUtils.*;

public class MainView {

    private static final NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance(new Locale.Builder().setLanguage("id").setRegion("ID").build());


    public void displayMenu(List<MenuItem> menu) {
        System.out.println(LINE);
        System.out.println("Bundo Kanduang");
        System.out.println(LINE);
        System.out.println("Menu:");
        System.out.printf("%-5s | %-15s | %-10s%n", "No.", "Nama Menu", "Harga");
        for (int i = 0; i < menu.size(); i++) {
            MenuItem item = menu.get(i);
            System.out.printf("%-5d | %-15s | %s%n", (i + 1), item.getName(), currencyFormatter.format(item.getPrice()));
        }

        System.out.println("99. Pesan dan Bayar");
        System.out.println("0. Keluar");
    }

    public void displayConfirmation(OrderItem orderedItem) {
        System.out.println("Order Confirmation:");
        System.out.println("Item: " + orderedItem.getMenuItem().getName());
        System.out.println("Quantity: " + orderedItem.getQuantity());
        System.out.println("Total Price: Rp" + currencyFormatter.format((long) orderedItem.getMenuItem().getPrice() * orderedItem.getQuantity()));
    }

    public void displayOrderConfirmation(List<OrderItem> orderedItems) {
        int totalBill = 0;
        System.out.println("Konfirmasi & Pembayaran");
        printOrderHeaders();
        for (OrderItem item : orderedItems) {
            int totalPrice = item.getMenuItem().getPrice() * item.getQuantity();
            totalBill += totalPrice;
            System.out.printf(ORDER_FORMAT, item.getMenuItem().getName(), item.getQuantity(), currencyFormatter.format(totalPrice));
        }
        System.out.println("----------------------------------------------- +");
        System.out.printf(BILL_FORMAT, TOTAL_BILL_LABEL, currencyFormatter.format(totalBill));
        System.out.println(" ");
        System.out.println("1. Konfirmasi dan Bayar");
        System.out.println("2. Kembali ke menu utama");
        System.out.println("3. Keluar aplikasi");
    }

    private void printOrderHeaders() {
        System.out.printf(ORDER_FORMAT, MENU_LABEL, QUANTITY_LABEL, TOTAL_PRICE_LABEL);
    }

    public void displayPaymentConfirmation() {
        System.out.println("Pembayaran berhasil. Terima kasih!");
    }

    public void displayReceipt(List<OrderItem> orders) {
        int totalBill = 0;
        System.out.println(LINE);
        System.out.println("Bundo Kanduang");
        System.out.println(LINE);
        System.out.println("Terima kasih sudah memesan di Bundo Kanduang\n");
        System.out.println("Dibawah ini adalah pesanan anda:\n");
        printOrderHeaders();
        for (OrderItem order : orders) {
            int totalPrice = order.getMenuItem().getPrice() * order.getQuantity();
            totalBill += totalPrice;
            System.out.printf(ORDER_FORMAT, order.getMenuItem().getName(), order.getQuantity(), currencyFormatter.format(totalPrice));
        }
        System.out.println("----------------------------------------------- +");
        System.out.printf(BILL_FORMAT, TOTAL_BILL_LABEL, currencyFormatter.format(totalBill));
        System.out.println("\nKonfirmasi Pembayaran");
    }

    public void saveReceiptToFile(List<OrderItem> orders) {
        int totalBill = 0;
        try (FileWriter writer = new FileWriter("receipt.txt")) {
            writer.write("===============================\n");
            writer.write("Bundo Kanduang\n");
            writer.write("===============================\n");
            writer.write("Terima kasih sudah memesan di Bundo Kanduang\n\n");
            writer.write("Dibawah ini adalah pesanan anda:\n");
            writer.write(String.format(ORDER_FORMAT, "MENU", "JUMLAH", "TOTAL HARGA"));
            for (OrderItem order : orders) {
                int totalPrice = order.getMenuItem().getPrice() * order.getQuantity();
                totalBill += totalPrice;
                writer.write(String.format(ORDER_FORMAT, order.getMenuItem().getName(), order.getQuantity(), currencyFormatter.format(totalPrice)));
            }
            writer.write("----------------------------------------------- +\n");
            writer.write(String.format(BILL_FORMAT, TOTAL_BILL_LABEL, currencyFormatter.format(totalBill)));
            writer.write("\nKonfirmasi Pembayaran\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
