package controller;

import model.MenuItem;
import model.OrderItem;
import view.MainView;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;


public class MainController {

    private final List<MenuItem> menu;
    private final List<OrderItem> orders;
    private final MainView mainView;

    public MainController() {
        this.menu = new ArrayList<>();
        this.orders = new ArrayList<>();
        this.mainView = new MainView();
    }

    public void start() {
        initializeMenu();
        Scanner scanner = new Scanner(System.in);

        while (true) {
            mainView.displayMenu(menu);
            int choice = getIntegerInput(scanner, "Enter your choice: ");

            switch (choice) {
                case 0:
                    System.out.println("Exiting the application...");
                    return;
                case 99:
                    confirmAndDisplayReceipt();
                    return;
                default:
                    handleMenuItemSelection(scanner, choice);
                    break;
            }
        }
    }

    private void handleMenuItemSelection(Scanner scanner, int choice) {
        if (choice < 1 || choice > menu.size()) {
            System.out.println("Invalid choice. Please try again.");
            return;
        }

        MenuItem selectedMenuItem = menu.get(choice - 1);
        int quantity = getQuantityInput(scanner);

        if (quantity == 0) {
            System.out.println("Order cancelled. Returning to main menu.");
            return;
        }

        updateOrderItems(selectedMenuItem, quantity);
        mainView.displayConfirmation(orders.getLast());

        if (!askToOrderMore(scanner)) {
            displayConfirmationMenu();
        }
    }

    private int getIntegerInput(Scanner scanner, String message) {
        while (true) {
            System.out.print(message);
            try {
                return scanner.nextInt();
            } catch (InputMismatchException e) {
                System.out.println(utils.StringUtils.INVALID_INPUT_NUMBER);
                scanner.next();
            }
        }
    }

    private int getQuantityInput(Scanner scanner) {
        return getIntegerInput(scanner, "Jumlah (Masukkan 0 untuk kembali): ");
    }

    private boolean askToOrderMore(Scanner scanner) {
        while (true) {
            System.out.print("Do you want to order more? (Y/N): ");
            String confirm = scanner.next();

            if (confirm.equalsIgnoreCase("Y")) {
                return true;
            } else if (confirm.equalsIgnoreCase("N")) {
                return false;
            } else {
                System.out.println("Invalid input. Please enter only Y/N.");
                scanner.nextLine(); // Membersihkan input buffer
            }
        }
    }

    private void updateOrderItems(MenuItem selectedMenuItem, int quantity) {
        boolean itemExists = false;
        for (OrderItem orderItem : orders) {
            if (orderItem.getMenuItem().equals(selectedMenuItem)) {
                orderItem.setQuantity(orderItem.getQuantity() + quantity);
                itemExists = true;
                break;
            }
        }

        if (!itemExists) {
            orders.add(new OrderItem(selectedMenuItem, quantity));
        }
    }

    private void displayConfirmationMenu() {
        while (true) {
            mainView.displayOrderConfirmation(orders);

            Scanner scanner = new Scanner(System.in);
            System.out.print("Enter your choice: ");
            if (scanner.hasNextInt()) {
                int choice = scanner.nextInt();

                switch (choice) {
                    case 1:
                        confirmAndDisplayReceipt();
                        mainView.saveReceiptToFile(orders);
                        System.exit(0);
                        return;
                    case 2:
                        return; // Return to main menu
                    case 3:
                        System.out.println("Exiting the application...");
                        System.exit(0);
                        return;
                    default:
                        System.out.println("Invalid choice. Please try again.");
                }
            } else {
                System.out.println("Invalid input. Please enter a number.");
                scanner.next();
            }
        }
    }

    private void confirmAndDisplayReceipt() {
        if (orders.isEmpty()) {
            System.out.println("No orders placed.");
            return;
        }

        mainView.displayReceipt(orders);
        mainView.displayPaymentConfirmation();
    }

    private void initializeMenu() {
        menu.add(new MenuItem("Nasi Padang", 20000));
        menu.add(new MenuItem("Nasi Rendang", 25000));
        menu.add(new MenuItem("Ayam Pop", 23000));
    }
}
