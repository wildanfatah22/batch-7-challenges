package utils;

public class StringUtils {
    public static final String ORDER_FORMAT = "%-20s %-10s %s%n";
    public static final String BILL_FORMAT = "%-31s %s%n";
    public static final String LINE = "===================================";
    public static final String TOTAL_BILL_LABEL = "Total Bill: ";
    public static final String MENU_LABEL = "MENU";
    public static final String QUANTITY_LABEL = "JUMLAH";
    public static final String TOTAL_PRICE_LABEL = "TOTAL HARGA";
    public static final String INVALID_INPUT_NUMBER = "Invalid input. Please enter a number.";

    private StringUtils() {
    }
}
